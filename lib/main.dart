import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import './pages/services/menu_services_page.dart';
import './pages/profile/menu_profile_page.dart';
import './scoped-models/services_model.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final ServicesModel _model = ServicesModel();
  int _selectedPage = 1;

  @override
  Widget build(BuildContext context) {
    final pageOptions = [
      Text(
        'Blog',
        style: TextStyle(fontSize: 36.0),
      ),
      MenuServicesPage(_model),
      MenuProfilePage(),
    ];

    return ScopedModel<ServicesModel>(
      model: _model,
      child: MaterialApp(
        title: 'Abroad Pal',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          body: Center(
            child: pageOptions[_selectedPage],
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _selectedPage,
            fixedColor: Colors.blueAccent,
            onTap: (int index) {
              setState(() {
                _selectedPage = index;
              });
            },
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.book),
                title: Text('Blog'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.call_to_action),
                title: Text('Services'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title: Text('Settings'),
              ),
            ],
          ),
        ),
        // home: App(),
        // routes: {
        //   '/': (BuildContext context) => MenuServicesPage(),
        //   '/agent': (BuildContext context) => AgentDetailsPage(),
        // },
      ),
    );
  }
}
