import 'package:flutter/material.dart';

import '../../models/consultant_bid.dart';

class AgentDetailsPage extends StatelessWidget {
  final ConsultantBid bid;

  AgentDetailsPage(this.bid);

  Widget _buildButtonAppointment(context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Text(
            'Schedule Appointment',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        onPressed: () {},
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _buildCardMainInfo() {
    return Card(
      margin: EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 10.0,
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 15.0,
        ),
        child: Column(
          children: <Widget>[
            Text(
              bid.consultant,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18.0,
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Text(
              bid.location,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 15.0,
                color: Colors.grey,
              ),
            ),
            SizedBox(
              height: 18.0,
            ),
            Text(
              bid.title,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 25.0,
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.star,
                  color: Colors.yellow,
                ),
                SizedBox(
                  width: 3.0,
                ),
                Text(
                  bid.rating.toString(),
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 15.0,
                  ),
                ),
                SizedBox(
                  width: 25.0,
                ),
                Text(
                  '${bid.reviews} Jobs Completed',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 18.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  '\$${bid.price}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25.0,
                    color: Colors.blue,
                  ),
                ),
                SizedBox(
                  width: 5.0,
                ),
                Text(
                  '/hr',
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCardDescriptionInfo() {
    return Card(
      child: Container(
        margin: EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 10.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(
                vertical: 10.0,
                horizontal: 5.0,
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.star,
                        size: 20.0,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        'Bullet point 1',
                        style: TextStyle(fontSize: 16.0),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.star,
                        size: 20.0,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        'Bullet point 2',
                        style: TextStyle(fontSize: 16.0),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.star,
                        size: 20.0,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        'Bullet point 3',
                        style: TextStyle(fontSize: 16.0),
                      )
                    ],
                  ),
                  Divider(),
                  Text(
                    bid.description,
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                  SizedBox(
                    height: 62.0,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 200.0,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Hero(
                tag: bid.id,
                child: Image.network(bid.image),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              _buildCardMainInfo(),
              _buildButtonAppointment(context),
              _buildCardDescriptionInfo(),
            ]),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(
          Icons.chat,
          color: Theme.of(context).primaryColor,
        ),
        label: Text(
          'chat',
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        backgroundColor: Colors.white,
        onPressed: () {},
        heroTag: 'chat',
      ),
    );
  }
}
