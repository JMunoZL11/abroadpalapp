import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../widgets/ui_elements/service_card.dart';
import '../../models/consultant_bid.dart';
import '../../scoped-models/services_model.dart';
import './agent_details_page.dart';

class MenuServicesPage extends StatefulWidget {
  final ServicesModel model;

  MenuServicesPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _MenuServicesPageState();
  }
}

class _MenuServicesPageState extends State<MenuServicesPage> {
  @override
  void initState() {
    widget.model.fetchServices();
    super.initState();
  }

  Widget _buildConsultantList() {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, ServicesModel model) {
        final List<ConsultantBid> _bids = model.consultantBids;
        return ListView.builder(
          itemCount: model.consultantBids.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                // model.selectService(_bids[index].id);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AgentDetailsPage(_bids[index])),
                );
                // Navigator.pushNamed(context, '/agent');
              },
              child: ServiceCard(_bids[index]),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Migration Services'),
          actions: <Widget>[
            Center(
              child: Text(
                'FILTER',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
            ),
          ],
        ),
        body: _buildConsultantList());
  }
}
