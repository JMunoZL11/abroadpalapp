import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/services_model.dart';
import '../../main.dart';

class ServiceForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ServiceFormState();
  }
}

class _ServiceFormState extends State<ServiceForm> {
  final Map<String, dynamic> _formData = {
    'title': null,
    'description': null,
    'location': null,
    'price': null,
  };

  String _dropdownValue;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildTitleTextField() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Service Title'),
      // initialValue: product == null ? '' : product.title,
      validator: (String value) {
        if (value.isEmpty || value.length < 5) {
          return 'Title is required and should be 5+ characters long.';
        }
      },
      onSaved: (String value) {
        _formData['title'] = value;
      },
    );
  }

  Widget _buildDescriptionTextField() {
    return TextFormField(
      maxLines: 4,
      decoration: InputDecoration(labelText: 'Service Description'),
      // initialValue: product == null ? '' : product.description,
      validator: (String value) {
        if (value.isEmpty || value.length < 10) {
          return 'Description is required and should be 10+ characters long.';
        }
      },
      onSaved: (String value) {
        _formData['description'] = value;
      },
    );
  }

  Widget _buildLocationDropDown() {
    return DropdownButtonFormField(
      decoration: InputDecoration(labelText: 'Location'),
      value: _dropdownValue,
      onChanged: (value) {
        setState(() {
          _dropdownValue = value;
        });
      },
      items: <DropdownMenuItem>[
        DropdownMenuItem(
          child: Row(
            children: <Widget>[Text('Australia'), Icon(Icons.flag)],
          ),
          value: 'Australia',
        ),
        DropdownMenuItem(
          child: Row(
            children: <Widget>[Text('Colombia'), Icon(Icons.flag)],
          ),
          value: 'Colombia',
        ),
      ],
      onSaved: (value) {
        _formData['location'] = value;
      },
    );
  }

  Widget _buildPriceTextField() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Product Price'),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      // initialValue: product == null ? '' : product.price.toString(),
      validator: (String value) {
        if (value.isEmpty ||
            RegExp(r'^(?:[1-9]\d*|0)?(?:[.,]\d+)?$').hasMatch(value) == false) {
          return 'Price is required and should be a number';
        }
      },
      onSaved: (String value) {
        _formData['price'] = double.parse(value);
      },
    );
  }

  Widget _buildSubmitButton() {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, ServicesModel model) {
        return RaisedButton(
          child: Text('Save'),
          color: Theme.of(context).primaryColor,
          textColor: Colors.white,
          onPressed: () => _submitForm(model.addService),
        );
      },
    );
  }

  _submitForm(Function addService) {
    if (_formKey.currentState.validate() == false) {
      return;
    }
    _formKey.currentState.save();
    addService(
      _formData['title'],
      _formData['description'],
      _formData['location'],
      _formData['price'],
    ).then(
      (bool success) {
        if (success) {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Success'),
                content: Text('Service successfuly added'),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MyApp(),
                          ),
                        ),
                    child: Text('Ok'),
                  ),
                ],
              );
            },
          );
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Something went wrong'),
                content: Text('Please try again!'),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text('Ok'),
                  ),
                ],
              );
            },
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Service'),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
        // margin: EdgeInsets.all(10.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            // padding: EdgeInsets.symmetric(horizontal: targetPadding / 2),
            child: Column(
              children: <Widget>[
                _buildTitleTextField(),
                _buildDescriptionTextField(),
                _buildLocationDropDown(),
                _buildPriceTextField(),
                SizedBox(
                  height: 10.0,
                ),
                _buildSubmitButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
