import 'package:flutter/material.dart';

import './service_form.dart';

class MenuProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 5.0,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ServiceForm()));
            },
            child: Container(
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                'Post service',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
          ),
          Divider(),
          Container(
            child: Text('Post service'),
          ),
          Divider(),
          Container(
            child: Text('Post service'),
          ),
          Divider(),
        ],
      ),
    );
  }
}
