import 'package:flutter/material.dart';

import '../../models/consultant_bid.dart';

class ServiceCard extends StatelessWidget {
  final ConsultantBid bid;

  ServiceCard(this.bid);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Row(
          children: <Widget>[
            Hero(
              tag: bid.id,
              child: CircleAvatar(
                child: Image.network(bid.image),
                radius: 50.0,
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  bid.title,
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  bid.consultant,
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
                Text(
                  bid.location,
                  style: TextStyle(
                    fontSize: 17.0,
                    color: Colors.grey,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.star,
                      color: Colors.yellow,
                    ),
                    SizedBox(
                      width: 3.0,
                    ),
                    Text(
                      bid.rating.toString(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text('(${bid.reviews})'),
                  ],
                ),
                Text(
                  '\$${bid.price} /hr',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
