class ConsultantBid{
  final String id;
  final String consultant;
  final String title;
  final String location;
  final double rating;
  final int reviews;
  final double price;
  final List<String> bullets;
  final String description;
  final String image;

  ConsultantBid(
      {this.id,
      this.consultant,
      this.title,
      this.location,
      this.rating,
      this.reviews,
      this.price,
      this.bullets,
      this.description,
      this.image});
}
