import 'dart:convert';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;

import '../models/consultant_bid.dart';

class ServicesModel extends Model {
  List<ConsultantBid> _consultantBids = [];
  String _selServiceId;

  List<ConsultantBid> get consultantBids {
    return _consultantBids;
  }

  Future<bool> addService(
      String title, String description, String location, double price) async {
    final Map<String, dynamic> serviceData = {
      'consultant': 'User Name',
      'title': title,
      'location': location,
      'rating': 0.0,
      'reviews': 0,
      'price': price,
      'description': description,
      'image':
          'https://cdn.pixabay.com/photo/2015/03/04/22/35/head-659652_960_720.png',
    };

    try {
      final http.Response response = await http.post(
          'https://abroad-pal.firebaseio.com/services.json?',
          body: json.encode(serviceData));

      if (response.statusCode != 200 && response.statusCode == 201) {
        // _isLoading = false;
        notifyListeners();
        return false;
      }
      return true;
    } catch (error) {
      return false;
    }
  }

  Future<Null> fetchServices() async {
    try {
      http.Response response =
          await http.get('https://abroad-pal.firebaseio.com/services.json?');
      final List<ConsultantBid> fetchedBidList = [];
      final serviceListData = json.decode(response.body);
      if (serviceListData == null) {
        notifyListeners();
        return;
      }
      serviceListData.forEach((String serviceId, dynamic serviceData) {
        final ConsultantBid consultantBid = ConsultantBid(
          id: serviceId,
          consultant: serviceData['consultant'],
          title: serviceData['title'],
          location: serviceData['location'],
          rating: serviceData['rating'],
          reviews: serviceData['reviews'],
          bullets: serviceData['bullets'],
          description: serviceData['description'],
          image: serviceData['image'],
          price: serviceData['price'],
        );
        fetchedBidList.add(consultantBid);
      });
      _consultantBids = fetchedBidList;
      notifyListeners();
    } catch (error) {
      return;
    }
  }

  void selectService(String serviceId) {
    _selServiceId = serviceId;
    if (serviceId != null) {
      notifyListeners();
    }
  }
}
